FROM openjdk:11.0.3-jre

EXPOSE 8080

ADD target/demo-0.1.jar /usr/local/demo/server.jar

CMD ["java", "-jar", "/usr/local/demo/server.jar"]
