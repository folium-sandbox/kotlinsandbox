# Covid statistic application

A simple application that shows statistics about the coronavirus by country.

## Technologies

- Kotlin
- Spring Boot
- Spek
- Maven
- Docker
