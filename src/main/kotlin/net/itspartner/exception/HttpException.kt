package net.itspartner.exception

data class HttpException(override val message: String?, val statusCode: Int) : Exception(message)
