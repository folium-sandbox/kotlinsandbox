package net.itspartner.exception.handler

import java.util.*

data class ExceptionDetails(val time: Date, val message: String, val statusCode: Int, val details: String)
