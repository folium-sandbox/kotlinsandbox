package net.itspartner.exception.handler

import mu.KotlinLogging
import net.itspartner.exception.HttpException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.util.*

private val klogger = KotlinLogging.logger {}

@ControllerAdvice
class ControllerAdviceRequestError : ResponseEntityExceptionHandler() {

    @ExceptionHandler(value = [(HttpException::class)])
    fun handleHttpException(ex: HttpException, request: WebRequest): ResponseEntity<ExceptionDetails> {

        val exceptionDetails = ExceptionDetails(Date(), "Validation Failed", ex.statusCode, ex.message!!)
        klogger.debug { "Handling http exception with details: $exceptionDetails" }

        return ResponseEntity(exceptionDetails, HttpStatus.valueOf(exceptionDetails.statusCode))
    }
}
