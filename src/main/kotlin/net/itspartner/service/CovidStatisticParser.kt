package net.itspartner.service

import net.itspartner.model.CovidStatistic

interface CovidStatisticParser {

    fun parse(content: String): List<CovidStatistic>
}