package net.itspartner.service

import net.itspartner.exception.HttpException

interface HttpRequesterService {

    @Throws(HttpException::class)
    fun get(url: String): String
}
