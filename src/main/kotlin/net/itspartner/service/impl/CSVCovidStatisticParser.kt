package net.itspartner.service.impl

import mu.KotlinLogging
import net.itspartner.model.CovidStatistic
import net.itspartner.service.CovidStatisticParser
import org.apache.commons.csv.CSVFormat
import java.io.StringReader

private val klogger = KotlinLogging.logger {}

class CSVCovidStatisticParser : CovidStatisticParser {

    override fun parse(content: String): List<CovidStatistic> {

        val csvReader = StringReader(content)
        val csvParser = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(csvReader)
        val records = csvParser.records
        klogger.debug { "Csv content has been parsed for processing. Size of records: ${records.size}" }

        val stats = mutableListOf<CovidStatistic>()
        for (record in records) {
            val totalLatestCases = Integer.parseInt(record.get(record.size() - 1))
            val prevDayCases = Integer.parseInt(record.get(record.size() - 2))
            val covid = CovidStatistic(
                record.get("Province/State"),
                record.get("Country/Region"),
                totalLatestCases,
                totalLatestCases - prevDayCases)
            stats.add(covid)
        }

        klogger.debug { "Processed covid statistics data: numberOfElements=${stats.size}, content=$stats" }
        return stats
    }
}