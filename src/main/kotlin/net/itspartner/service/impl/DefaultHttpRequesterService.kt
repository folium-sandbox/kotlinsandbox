package net.itspartner.service.impl

import mu.KotlinLogging
import net.itspartner.exception.HttpException
import net.itspartner.service.HttpRequesterService
import org.springframework.http.HttpStatus
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.time.Duration

private val klogger = KotlinLogging.logger {}

class DefaultHttpRequesterService(
    private val client: HttpClient
) : HttpRequesterService {

    override fun get(url: String): String {
        klogger.debug { "Received url: $url" }

        val request = HttpRequest.newBuilder()
            .timeout(Duration.ofMillis(TIMEOUT))
            .uri(URI.create(url))
            .build()
        klogger.debug { "Http request has been built" }

        try {
            val response = client.send(request, HttpResponse.BodyHandlers.ofString())
            klogger.debug { "Received response for $response" }

            if (response.statusCode() != HttpStatus.OK.value()) {
                throw HttpException("$response", response.statusCode())
            }

            klogger.debug { "Response body: ${response.body()}" }
            return response.body()
        } catch (ex: Exception) {
            klogger.error { "Exception was occurred: ${ex.message}" }
            when (ex) {
                is HttpException -> { throw HttpException(ex.message, ex.statusCode) }
                else -> throw HttpException(ex.message, HttpStatus.BAD_REQUEST.value())
            }
        }
    }

    companion object {

        const val TIMEOUT = 2000L
    }
}
