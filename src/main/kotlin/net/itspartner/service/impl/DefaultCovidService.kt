package net.itspartner.service.impl

import mu.KotlinLogging
import net.itspartner.config.AppProperty
import net.itspartner.model.CovidStatistic
import net.itspartner.service.CovidService
import net.itspartner.service.CovidStatisticParser
import net.itspartner.service.HttpRequesterService

private val klogger = KotlinLogging.logger {}

class DefaultCovidService(
    private val httpRequester: HttpRequesterService,
    private val covidStatisticParser: CovidStatisticParser,
    private val appProperty: AppProperty
) : CovidService {

    override fun getStatistics(): List<CovidStatistic> {

        val apiUrl = appProperty.getValue("api.covid.stats")
        klogger.debug { "Getting covid statistics by api url: $apiUrl" }
        val body = httpRequester.get(apiUrl)

        return covidStatisticParser.parse(body)
    }
}
