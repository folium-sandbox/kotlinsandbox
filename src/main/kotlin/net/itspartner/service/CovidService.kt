package net.itspartner.service

import net.itspartner.model.CovidStatistic

interface CovidService {

    fun getStatistics(): List<CovidStatistic>
}
