package net.itspartner.config

import mu.KotlinLogging
import org.springframework.context.annotation.PropertySource
import org.springframework.core.env.Environment
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

private val klogger = KotlinLogging.logger {}

@Component
@PropertySource("classpath:application.properties")
class AppProperty(val env: Environment) {

    fun getValue(configKey: String): String {

        val property = env.getProperty(configKey)
        property?.let {
            klogger.debug { "Return property=$property for configKey=$configKey" }
            return property
        }
        throw NullPointerException("Property for configKey=$configKey does not exist")
    }

    @PostConstruct
    fun postConstruct() {
        klogger.info { "AppProperty bean has been loaded" }
    }
}
