package net.itspartner.config

import mu.KotlinLogging
import net.itspartner.service.impl.CSVCovidStatisticParser
import net.itspartner.service.impl.DefaultCovidService
import net.itspartner.service.impl.DefaultHttpRequesterService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.net.http.HttpClient
import javax.annotation.PostConstruct

private val klogger = KotlinLogging.logger {}

@Configuration
class MainConfig {

    @Autowired
    private lateinit var appProperty: AppProperty

    @Bean
    fun httpClient(): HttpClient = HttpClient.newBuilder().build()

    @Bean
    fun covidStatisticParser() = CSVCovidStatisticParser()

    @Bean
    fun httpRequesterService() = DefaultHttpRequesterService(client = httpClient())

    @Bean
    fun covidService() = DefaultCovidService(
        httpRequester = httpRequesterService(),
        covidStatisticParser = covidStatisticParser(),
        appProperty = appProperty
    )

    @PostConstruct
    fun postConstruct() {
        klogger.info { "All beans have been started" }
    }
}