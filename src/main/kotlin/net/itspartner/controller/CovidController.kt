package net.itspartner.controller

import net.itspartner.dto.CovidStatisticListDto
import net.itspartner.service.CovidService
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/covid")
class CovidController(private val covidService: CovidService) {

    @GetMapping("/stats")
    fun getCovidStatistic(model: Model): CovidStatisticListDto {

        return CovidStatisticListDto(covidService.getStatistics())
    }

}
