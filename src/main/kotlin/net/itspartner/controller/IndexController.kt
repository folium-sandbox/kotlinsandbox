package net.itspartner.controller

import net.itspartner.service.CovidService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.GetMapping

@Controller
// TODO: 10.03.21 add actuator spring for health check
class IndexController {

    @Autowired
    private lateinit var covidService: CovidService

    @GetMapping("/","/index")
    fun home(model: Model): String {

        val stats = covidService.getStatistics()
        model["covidStatistics"] = stats

        return "index"
    }
}
