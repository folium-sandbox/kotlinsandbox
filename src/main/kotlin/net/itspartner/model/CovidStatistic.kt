package net.itspartner.model

data class CovidStatistic(
    val state: String,
    val country: String,
    val totalLatestCases: Int,
    val diffFromPrevDay: Int
)
