package net.itspartner.utils

class ExceptionMessageUtils {

    companion object {

        const val TIMEOUT_EXCEPTION = "request timed out"
        const val URI_WITH_UNDEFINED_SCHEME = "URI with undefined scheme"
    }
}
