package net.itspartner.dto

import net.itspartner.model.CovidStatistic

data class CovidStatisticDto(
    val state: String,
    val country: String,
    val totalLatestCases: Int,
    val diffFromPrevDay: Int
) {
    constructor(covidStatistic: CovidStatistic)
            : this(covidStatistic.state,
                   covidStatistic.country,
                   covidStatistic.totalLatestCases,
                   covidStatistic.diffFromPrevDay)
}
