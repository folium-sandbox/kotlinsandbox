package net.itspartner.integration_test.utils

import java.nio.file.Paths

fun getAbsolutePath(): String {

    return Paths.get("").toAbsolutePath().toString()
}