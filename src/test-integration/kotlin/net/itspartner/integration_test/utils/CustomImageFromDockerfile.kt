package net.itspartner.integration_test.utils

import org.testcontainers.images.builder.ImageFromDockerfile

class CustomImageFromDockerfile : ImageFromDockerfile() {

    fun build() = resolve() // build image
}
