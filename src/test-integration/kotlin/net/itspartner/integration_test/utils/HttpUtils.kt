package net.itspartner.integration_test.utils

import net.itspartner.service.impl.DefaultHttpRequesterService
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.time.Duration

class HttpUtils(private val client: HttpClient) {

    fun get(url: String): HttpResponse<String> {

        val request = HttpRequest.newBuilder()
            .timeout(Duration.ofMillis(DefaultHttpRequesterService.TIMEOUT))
            .uri(URI.create(url))
            .build()

        return client.send(request, HttpResponse.BodyHandlers.ofString())
    }

}