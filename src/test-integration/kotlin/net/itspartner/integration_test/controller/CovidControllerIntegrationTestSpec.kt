package net.itspartner.integration_test.controller

import io.kotlintest.matchers.shouldBe
import io.kotlintest.matchers.shouldThrow
import net.itspartner.integration_test.IntegrationTestBase
import net.itspartner.integration_test.utils.HttpUtils
import net.itspartner.service.impl.DefaultHttpRequesterService
import net.itspartner.utils.FileLoaderUtils
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.mockserver.model.Delay
import org.mockserver.model.HttpRequest.request
import org.mockserver.model.HttpResponse.response
import org.springframework.http.HttpStatus
import org.testcontainers.shaded.org.apache.commons.lang.StringUtils
import java.net.http.HttpClient
import java.net.http.HttpTimeoutException

internal class CovidControllerIntegrationTestSpec : IntegrationTestBase({

    val client = HttpClient.newBuilder().build()
    val httpUtil = HttpUtils(client)
    val covidStatsApiUrl = "$appEndpoint/api/v1/covid/stats"

    beforeEachTest {
        mockServerClient.reset()
    }

    describe("#getCovidStatistic") {

        on("get json array of covid statistics") {

            val content = FileLoaderUtils.readAll("src/test/resources/time_series_covid19_one_country.csv")
            mockServerClient.`when`(
                request()
                    .withMethod("GET")
                    .withPath(mockApiPath)
            ).respond(
               response()
                    .withStatusCode(HttpStatus.OK.value())
                    .withBody(content))

            val response = httpUtil.get(covidStatsApiUrl)

            it("returns 200 status and statistics for one country") {

                response.statusCode() shouldBe HttpStatus.OK.value()
                val expectedBody = FileLoaderUtils.readAll("src/test-integration/resources/responses/GET_covid_stats_one_country.json")
                response.body() shouldBe expectedBody
            }
        }

        on("empty csv covid content") {

            mockServerClient.`when`(
                request()
                    .withMethod("GET")
                    .withPath(mockApiPath)
            ).respond(
                response()
                    .withStatusCode(HttpStatus.OK.value())
                    .withBody(StringUtils.EMPTY))

            val response = httpUtil.get(covidStatsApiUrl)

            it("returns 200 status and empty json") {

                response.statusCode() shouldBe HttpStatus.OK.value()
                val expectedBody = FileLoaderUtils.readAll("src/test-integration/resources/responses/GET_empty_covid_stats.json")
                response.body() shouldBe expectedBody
            }
        }

        on("covid stats api for csv content not-found") {

            mockServerClient.`when`(
                request()
                    .withMethod("GET")
                    .withPath(mockApiPath)
            ).respond(
                response()
                    .withStatusCode(HttpStatus.NOT_FOUND.value()))

            val response = httpUtil.get(covidStatsApiUrl)

            it("returns 404 not found status") {

                response.statusCode() shouldBe HttpStatus.NOT_FOUND.value()
            }
        }

        on("http request timeout error") {

            mockServerClient.`when`(
                request()
                    .withMethod("GET")
                    .withPath(mockApiPath)
            ).respond(
                response()
                    .withDelay(Delay.milliseconds(DefaultHttpRequesterService.TIMEOUT + 50)))

            it("returns request timeout error") {

                shouldThrow<HttpTimeoutException> {
                    httpUtil.get(covidStatsApiUrl)
                }
            }
        }

        on("rest api url not found error") {

            val response = httpUtil.get("$appEndpoint/not-found")

            it("returns 404 not found status") {

                response.statusCode() shouldBe HttpStatus.NOT_FOUND.value()
            }
        }
    }

})