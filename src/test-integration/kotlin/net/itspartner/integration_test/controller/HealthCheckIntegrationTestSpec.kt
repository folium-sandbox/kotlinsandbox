package net.itspartner.integration_test.controller

import io.kotlintest.matchers.shouldBe
import net.itspartner.integration_test.IntegrationTestBase
import net.itspartner.integration_test.utils.HttpUtils
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.springframework.http.HttpStatus
import java.net.http.HttpClient

internal class HealthCheckIntegrationTestSpec : IntegrationTestBase ({

    beforeEachTest { mockServerClient.reset() }

    describe("health check") {

        on("valid health check request") {

            val healthCheckEndpoint = "$appEndpoint/actuator/health"
            val response =  HttpUtils(HttpClient.newHttpClient()).get(healthCheckEndpoint)

            it("returns 200 ok") {

                response.statusCode() shouldBe HttpStatus.OK.value()
            }
        }
    }

})
