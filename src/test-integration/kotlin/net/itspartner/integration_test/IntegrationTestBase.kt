package net.itspartner.integration_test

import net.itspartner.integration_test.testcontainers.AppContainerStarter
import net.itspartner.integration_test.testcontainers.mockserver.MockServerContainerStarter
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.Spec
import org.mockserver.client.server.MockServerClient
import org.testcontainers.containers.Network

internal abstract class IntegrationTestBase(spec: Spec.() -> Unit) : Spek(spec) {

    companion object {

        var appEndpoint: String
        var mockServerClient: MockServerClient
        var mockServerEndpoint: String
        var mockServerInternalTestContainersEndpoint: String
        var mockApiPath = "/covid/stats/test"

        init {
            val commonContainersNetwork = Network.newNetwork()

            val mockServer = MockServerContainerStarter(commonContainersNetwork).startContainer()
            mockServerClient = mockServer.mockServerClient
            mockServerEndpoint = mockServer.internalContainerEndpoint
            mockServerInternalTestContainersEndpoint = mockServer.internalTestContainersEndpoint

            val appContainerInfo =
                AppContainerStarter(commonContainersNetwork, mapOf("api.covid.stats" to "${mockServer.internalTestContainersEndpoint}$mockApiPath"))
                    .startContainer()

            appEndpoint = appContainerInfo.getEndpoint()
        }
    }
}
