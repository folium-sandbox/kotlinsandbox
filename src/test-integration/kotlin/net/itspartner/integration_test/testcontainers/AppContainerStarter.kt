package net.itspartner.integration_test.testcontainers

import net.itspartner.integration_test.utils.CustomImageFromDockerfile
import net.itspartner.integration_test.utils.getAbsolutePath
import org.testcontainers.containers.Network
import java.io.File

class AppContainerStarter (
    private val network: Network,
    private val envVariables: Map<String, String>
) : ContainerStarter<AppContainerInfo> {

    override fun startContainer(): AppContainerInfo {
        val pathToModule = getAbsolutePath()
        println( "Path to request router module is $pathToModule" )

        val moduleImage = CustomImageFromDockerfile()
        moduleImage.withFileFromFile(
            "target/demo-0.1.jar",
            File("$pathToModule/target/demo-0.1.jar")
        )

        moduleImage.withFileFromFile(
            "Dockerfile",
            File("$pathToModule/Dockerfile")
        )
        moduleImage.build()

        val moduleContainer = AppContainer(moduleImage.dockerImageName)
        moduleContainer
            .withNetwork(network)
            .withEnv(envVariables)

        moduleContainer.start()

        return AppContainerInfo(
            endpoint = "http://${moduleContainer.containerIpAddress}:${moduleContainer.containerPort}"
        )
    }

}
