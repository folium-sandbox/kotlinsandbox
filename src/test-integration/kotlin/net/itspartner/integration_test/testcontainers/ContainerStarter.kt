package net.itspartner.integration_test.testcontainers

interface ContainerStarter<out R> {

    fun startContainer(): R
}
