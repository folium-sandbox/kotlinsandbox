package net.itspartner.integration_test.testcontainers

import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.wait.strategy.HttpWaitStrategy

class AppContainer(imageName: String) : GenericContainer<AppContainer>(imageName) {


    init {
        // Use to wait until app started
        waitStrategy = HttpWaitStrategy().forPath("/nonExisting").forPort(8080).forStatusCode(404)
    }
    val containerPort: Int
        get() = getMappedPort(8080)
}
