package net.itspartner.integration_test.testcontainers

class AppContainerInfo (
    private val endpoint: String
) {

    fun getEndpoint(): String = endpoint
}
