package net.itspartner.integration_test.testcontainers.mockserver

import org.mockserver.client.server.MockServerClient

data class MockServerContainerInfo(
    val endpoint: String,
    val internalTestContainersEndpoint: String,
    val mockServerClient: MockServerClient,
    val internalContainerEndpoint: String
)
