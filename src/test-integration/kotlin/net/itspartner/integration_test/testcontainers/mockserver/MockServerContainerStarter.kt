package net.itspartner.integration_test.testcontainers.mockserver

import net.itspartner.integration_test.testcontainers.ContainerStarter
import org.mockserver.client.server.MockServerClient
import org.testcontainers.Testcontainers
import org.testcontainers.containers.MockServerContainer
import org.testcontainers.containers.Network

/**
 * [useInternalHost] is needed to add opportunity to mockserver to accept requests from another container in the same network
 * */
class MockServerContainerStarter(
    private val network: Network?,
    private val useInternalHost: Boolean = true
) : ContainerStarter<MockServerContainerInfo> {

    override fun startContainer(): MockServerContainerInfo {

        val mockServerContainer = MockServerContainer()
        if (network != null) {
            mockServerContainer
                .withNetwork(network)
                .withNetworkAliases("origin.com")
                .withNetworkMode("host")
        }

        mockServerContainer.start()

        val host = if (useInternalHost) {
            // Some Jenkins cannot lookup hostnames so we use this hack
            // exposing mock container external port inside of our container https://www.testcontainers.org/features/networking/
            // access url will be like http://host.testcontainers.internal:PORT
            Testcontainers.exposeHostPorts(mockServerContainer.serverPort)

            "host.testcontainers.internal"
        } else {
            "${mockServerContainer.containerIpAddress}:${mockServerContainer.firstMappedPort}"
        }

        return MockServerContainerInfo(
            endpoint = "http://$host",
            internalTestContainersEndpoint = "http://$host:${mockServerContainer.serverPort}",
            mockServerClient = MockServerClient(mockServerContainer.containerIpAddress, mockServerContainer.serverPort),
            internalContainerEndpoint = "http://origin.com:80"
        )
    }

}
