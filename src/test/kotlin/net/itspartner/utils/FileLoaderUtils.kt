package net.itspartner.utils

import java.nio.file.Files
import java.nio.file.Paths

object FileLoaderUtils {

    fun readAll(path: String): String {

        return String(Files.readAllBytes(Paths.get(path)))
    }
}