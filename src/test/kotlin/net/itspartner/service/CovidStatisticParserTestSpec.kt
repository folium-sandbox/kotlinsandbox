package net.itspartner.service

import io.kotlintest.matchers.shouldBe
import net.itspartner.service.impl.CSVCovidStatisticParser
import net.itspartner.utils.FileLoaderUtils
import net.itspartner.utils.RandomUtils.randomStr
import org.apache.commons.lang3.StringUtils
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on

internal class CovidStatisticParserTestSpec: Spek({

    val content = FileLoaderUtils.readAll("src/test/resources/time_series_covid19_one_country.csv")

    describe("#parse") {

        val covidStatisticParser = CSVCovidStatisticParser()

        on("valid csv content") {

            val result = covidStatisticParser.parse(content)

            it("returns a covid statistic list of size 1") {

                result.size shouldBe 1
            }

            val expectedCountry = "Afghanistan"

            it("returns $expectedCountry country") {

                result[0].country shouldBe expectedCountry
            }
        }

        on("empty csv content") {

            val result = covidStatisticParser.parse(StringUtils.EMPTY)

            it("returns empty list") {

                result.isEmpty()
            }
        }

        on("non-valid csv content") {

            val result = covidStatisticParser.parse(randomStr(1))

            it("returns empty list") {

                result.isEmpty()
            }
        }
    }
})
