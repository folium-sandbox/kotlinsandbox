package net.itspartner.service

import io.kotlintest.matchers.shouldBe
import io.kotlintest.matchers.shouldThrow
import net.itspartner.exception.HttpException
import net.itspartner.service.impl.DefaultHttpRequesterService
import net.itspartner.utils.ExceptionMessageUtils.Companion.TIMEOUT_EXCEPTION
import net.itspartner.utils.ExceptionMessageUtils.Companion.URI_WITH_UNDEFINED_SCHEME
import net.itspartner.utils.RandomUtils
import net.itspartner.utils.RandomUtils.randomStr
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.mockserver.integration.ClientAndServer
import org.mockserver.model.Delay
import org.mockserver.model.HttpRequest.request
import org.mockserver.model.HttpResponse.response
import org.springframework.http.HttpStatus
import java.net.http.HttpClient

internal class HttpRequesterServiceTestSpec : Spek({

    val port = RandomUtils.randomPort()
    val baseUrl = "http://localhost:$port"
    val client = HttpClient.newBuilder().build()
    val mockServer = ClientAndServer.startClientAndServer(port)

    beforeEachTest { mockServer.reset() }

    afterGroup { mockServer.stop() }

    group("Testing http functionality") {

        describe("#get") {

            val httpRequester = DefaultHttpRequesterService(client)

            on("valid http request") {

                val apiUrl = "/covid/stats"
                val body = randomStr(5)
                mockServer.`when`(request()
                        .withPath(apiUrl)
                        .withMethod("GET")
                ).respond(response()
                        .withStatusCode(HttpStatus.OK.value())
                        .withBody(body))

                val responseBody = httpRequester.get("$baseUrl$apiUrl")

                it("returns response: $body") {

                    responseBody shouldBe body
                }
            }

            on("uri with undefined scheme") {

                it("throws IllegalArgumentException") {

                    val ex = shouldThrow<IllegalArgumentException> {
                        httpRequester.get("unknown")
                    }
                    ex.message shouldBe URI_WITH_UNDEFINED_SCHEME
                }
            }

            on("url not found") {

                val apiUrl = "$baseUrl/not-found"

                it("throws HttpException") {

                    val ex = shouldThrow<HttpException> {
                        httpRequester.get(apiUrl)
                    }
                    ex.message shouldBe "(GET $apiUrl) 404"
                }
            }

            on("timeout exception") {

                val apiUrl = "/covid/timeout"
                mockServer.`when`(request()
                        .withPath(apiUrl)
                        .withMethod("GET")
                ).respond(response()
                        .withDelay(Delay.milliseconds(DefaultHttpRequesterService.TIMEOUT + 50)))

                it("throws HttpException") {

                    val ex = shouldThrow<HttpException> {
                        httpRequester.get("$baseUrl$apiUrl")
                    }
                    ex.message shouldBe TIMEOUT_EXCEPTION
                }
            }
        }
    }
})
