package net.itspartner.service

import com.nhaarman.mockito_kotlin.mock
import io.kotlintest.matchers.shouldBe
import io.kotlintest.matchers.shouldThrow
import net.itspartner.config.AppProperty
import net.itspartner.model.CovidStatistic
import net.itspartner.service.impl.DefaultCovidService
import net.itspartner.utils.FileLoaderUtils
import net.itspartner.utils.RandomUtils
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.mockito.Mockito.`when`
import org.mockito.Mockito.reset

internal class CovidServiceTestSpec : Spek({

    val httpRequester = mock<HttpRequesterService>()
    val covidStatisticParser = mock<CovidStatisticParser>()
    val appProperty = mock<AppProperty>()
    val covidService = DefaultCovidService(httpRequester, covidStatisticParser, appProperty)

    beforeEachTest {
        reset(httpRequester, covidStatisticParser, appProperty)
    }

    describe("#getStatistics") {

        on("http gets csv content and covid parser parses it") {

            val apiUrl = "http://covid-stats.com"
            val country = RandomUtils.randomStr(5)
            `when`(appProperty.getValue("api.covid.stats")).thenReturn(apiUrl)
            val content = FileLoaderUtils.readAll("src/test/resources/time_series_covid19_one_country.csv")
            `when`(httpRequester.get(apiUrl)).thenReturn(content)
            val covidStatistic = CovidStatistic("", country, 300, 200)
            `when`(covidStatisticParser.parse(content)).thenReturn(listOf(covidStatistic))

            val result = covidService.getStatistics()

            it("returns a covid statistic list of size 1") {

                result.size shouldBe 1
            }

            it("returns covidStatistic object") {

                result[0] shouldBe covidStatistic
            }
        }

        on("non-existing configKey") {

            `when`(appProperty.getValue("api.covid.stats"))
                .thenThrow(NullPointerException())

            it("throws NullPointerException") {

                shouldThrow<NullPointerException> {
                    covidService.getStatistics()
                }
            }
        }
    }
})
